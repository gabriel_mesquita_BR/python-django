mylist = [1, 2, 3, True]

print(len(mylist))

mylist.append("New Item")

print(mylist)

mylist.append(["x", "y", "z"])

print(mylist)

# ====================================== #

mylist2 = [4, 5, 6]
mylist2.extend(["x", "y", "z"])

print(mylist2)

# ====================================== #

# lists comprehensions

# tenho 3 arrays dentro de um array ou 3 listas dentro de uma lista
# mylist3[0][0] será o valor 1
mylist3 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# isso é um list comprehension, pois um loop ocorre dentro de uma lista
first_col = [row[0] for row in mylist3]

print(first_col)