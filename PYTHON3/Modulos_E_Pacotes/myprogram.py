# import mymodule

# mymodule.func_in_module()

# import mymodule as modulo

# modulo.func_in_module()

# importo somente a função do módulo
# from mymodule import func_in_module

# func_in_module()

# esta forma de importar abaixo não se deve fazer, pois consome mt memória, já que estou importando td do módulo
from mymodule import *
func_in_module()