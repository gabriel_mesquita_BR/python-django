s = "GLOBAL VARIABLE"

def func():
    # global s
    # s = 5000 # assim a variável global deixa de ser string para ser inteiro ou número
    mylocal = 10
    print(locals()) # printa as variáveis locais
    # print(globals()) # printa as variáveis globais
    print(globals()['s']) # printa somente a variável global deste arquivo

func()
# print(s)

# ======================================================================================================= #

def hello(name="jax"):
    print("Hello function")

    def greet():
        return "Greet function"

    def welcome():
        return "Welcome function"

    if name == "jax":
        return greet
    else:
        return welcome

x = hello() # a execução do método hello retorna uma função
print(x()) # como x está armazenando uma função acrescento () depois do x para executar esta função

# ======================================================================================================= #

def helloName():
    return "Hi Jack"

def other(func): # passo como parâmetro uma função
    print(func()) # printa a função que foi passada como parâmetro

other(helloName)

# ======================================================================================================= #

def new_decorator(func):
    def wrap_func():
        print("Antes de Func")
        func()
        print("Depois de Func")
    
    return wrap_func

# def func_needs_decorator():
#    print("This function is in need of a decorator!")

# new_decorator retorna a função wrap_func
# func_needs_decorator = new_decorator(func_needs_decorator)

# func_needs_decorator armazena a função wrap_func, logo acrescento () no func_needs_decorator para executar a
# função wrap_func
# func_needs_decorator()

# porém existe uma forma de fazer esses passos acima de uma forma mais fácil, usando DECORATOR

@new_decorator
def func_needs_decorator():
    print("This function is in need of a decorator!")

func_needs_decorator()