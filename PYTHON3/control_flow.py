if 1==1:
    if 1 > 2:
        print("hello")
    elif 3 == 3:
        print("elif ran")
    else:
        print("last")

lista = [1, 2, 3, 4, 5]

for item in lista:
    print(item)

dictionary = {"Sam":1, "Frank":2, "Dan":3}

for item in dictionary:
    print(item)
    print(dictionary[item])

mypairs = [(1,2), (3,4), (5,6)]

for item in mypairs:
    print(item)

for (tupla1, tupla2) in mypairs:
    print(tupla1) # printa o primeiro valor de cada tupla
    print(tupla2) # printa o segundo e último valor de cada tupla

cont = 1

while cont < 5:
    print("O valor é: {}".format(cont))
    cont = cont +1

for item in range(10):
    print(item)