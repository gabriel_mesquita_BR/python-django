import re # importo a biblioteca de expressão regular

patterns = ['term1', 'term2']

text = "Isto é uma string com term1, sem outro!"

for pattern in patterns:
    # busco por pattern (term1, term2) no text
    if re.search(pattern, text):
        print("Match!")

match = re.search('term1', text)
print(match.start()) # retorna o índice em que term1 foi encontrado em text

split_term = "@"
email = "user@gmail.com"
match = re.split(split_term, email)
print(match)

# retorna em uma lista (array) a palavra match de acordo com a quantidade de vezes em que ela aparecer na string
# logo no caso abaixo match aparecerá 2 vezes na lista
print(re.findall('match', 'test phrase match in match middle'))

# quero procurar na string pelo caractere "a" seguido por 0 ou mais caracteres "t"
print(re.findall('at*', 'test phrase match in match middle'))

# quero procurar na string pelo caractere "a" seguido por 1 ou mais caracteres "t"
print(re.findall('at+', 'test phrase match in match middle'))

# quero procurar na string pelo caractere "a" seguido por 0 ou 1 caractere "t"
print(re.findall('at?', 'test phrase mattch in match middle'))

# quero procurar na string pelo caractere "a" seguido por 2 caracteres "t"
print(re.findall('at{2}', 'test phrase mattch in match middle'))

# quero procurar na string pelo caractere "a" seguido por 1 ou 2 caracteres "t"
print(re.findall('at{1,2}', 'test phrase mattch in match middle'))

# quero procurar na string pelo caractere "s" seguido por 1 ou mais caracteres "sd"
print(re.findall('s[sd]+', 'sdss..ssssssddddd..ssssdddd..dsssd..ddddds'))

# se o acento circunflexo estiver fora dos colchetes estou dizendo que quero que comece com o q estiver dentro dos
# colchetes, se o acento circunflexo estiver dentro dos colchetes estou dizendo que queremo remover o que vem depois
print(re.findall('[^!.?]+', 'This is a string! But is has punctuation. How can we remove?'))

# pego os caracteres maiúsculos de A-Z que estiverem na string
print(re.findall('[A-Z]+', 'This is a string! But is has punctuation. How can we remove?'))

print(re.findall('[a-z]+', 'This is a string! But is has punctuation. How can we remove?'))

# pega 1 ou mais números
print(re.findall(r'\d+', 'Text with numbers: 1, 2, 3, 4, 5'))

# pega 1 ou mais caracteres que não forem dígitos
print(re.findall(r'\D+', 'Text with numbers: 1, 2, 3, 4, 5'))

# pega os whitespaces (espaços em brancos)
print(re.findall(r'\s+', 'Text with numbers: 1, 2, 3, 4, 5'))

# não pega os whitespaces
print(re.findall(r'\S+', 'Text with numbers: 1, 2, 3, 4, 5'))

# pega os alfanuméricos (letras e números)
print(re.findall(r'\w+', 'Text with numbers: 1, 2, 3, 4, 5'))

# pega os que não são alfanuméricos
print(re.findall(r'\W+', 'Text with numbers: 1, 2, 3, 4, 5, e #'))