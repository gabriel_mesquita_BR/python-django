try:
    f = open("sample.txt", "w")
    f.write("Python")

except IOError: # except é como se fosse o catch do JAVA, é onde o erro se tiver ocorrido será capturado 
    print("Erro: Arquivo não encontrado ou não foi possível escrever algo nele")

else:
    print("Sucesso")
    f.close()

try:
    f = open("sample.txt", "w")
    f.write("Php")

except IOError:
    print("Erro: Arquivo não encontrado ou não foi possível escrever algo nele")

finally: # msm se ocorrer erro o bloco de código dentro de finally será executado
    print("Entrou no finally")
    f.close()