def my_func(param1="default"):
    print("Meu parâmetro: {}".format(param1))

my_func()

def addNum(num1, num2):
    if type(num1) == type(num2):
        return num1 + num2

valor = addNum(3, 4)
print(valor)

mylist = [1, 2, 3, 4, 5, 6 , 7, 8, 9, 10]

def even_bool(num):
    return num % 2 == 0

evens = filter(even_bool, mylist) # retorna os números que são pares

print(list(evens)) # converto pra list

# função lambda também é chamado de função anônima, pois não tem nome
# num é o parâmetro da função lambda
results = filter(lambda num:num % 2 != 0, mylist)
print(list(results))

print('x' in [1, 2, 3])