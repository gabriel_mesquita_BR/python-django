class Animal():
    def __init__(self):
        print("Animal Criado")

    def whoAmI(self):
        print("Animal")

    def eat(self):
        print("Comendo")

class Dog(Animal): # classe Dog extende (herda) a classe Animal

    def __init__(self):
        Animal.__init__(self)
        print("Cachorro Criado")

    def bark(self):
        print("WOOF")
    
    def eat(self):
        print("Cachorro Comendo")

dog = Dog()

dog.whoAmI()
dog.eat()
dog.bark()