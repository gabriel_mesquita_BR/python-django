class Book():

    # estes método que começam e terminam com "__" são métodos do python chamados métodos especiais

    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages

    # este método é a representação de string da classe Book
    # é o toString() do JAVA
    def __str__(self):
        return "Título: {}, Autor: {}, Páginas: {}".format(self.title, self.author, self.pages)

    # este método é padrão do python
    def __len__(self):
        return self.pages

    def __del__(self):
        print("Livro foi deletado")

book = Book("Python", "Random", 234)
print(book)
print(len(book)) # retorna o número de páginas

del book # deleta o objeto book da memória