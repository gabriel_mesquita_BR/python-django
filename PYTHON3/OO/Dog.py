# Programação Orientada a Objetos

class Dog():

    # atributo da classe
    species="mamífero"

    def __init__(self, breed, name):
        self.breed = breed
        self.name  = name

x = Dog(breed = "Lab", name="Au Au")

y = Dog("Lab", "Au Au")

print(x.breed)
print(x.name)

print("=====================")

print(y.breed)
print(y.name)
print(y.species)