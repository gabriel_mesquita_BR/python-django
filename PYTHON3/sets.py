# É uma coleção de elementos únicos ordenados, ou seja, não repete elementos

x = set()

x.add(1)
x.add(2)
x.add(1)
x.add(0.5)

print(x)

converted = set([1, 1, 1, 1, 4, 4, 6, 2, 0.4, 10, 10, 10])

print(converted)    