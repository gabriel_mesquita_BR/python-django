mystring = 'abcdef'
print(mystring[2:]) # pego os caracteres a partir do caractere de índice 2
print(mystring[:3]) # pego os caracteres até o caractere de índice 2
print(mystring[::2]) # pego os caracteres em que o step size é 2, assim ele pega o primeiro caractere anda dois 
                     # caracteres e pega o caractere e assim por diante

print(mystring.upper)
print(mystring.lower)

# interpolação
print("Essa é minha string: {}".format(mystring))

print("Item 1: {}, Item 2: {}".format("jogos", "séries"))

print("Item 1: {x}, Item 2: {y}".format(x = "cachorro", y = "gato"))