from django.urls import path

from . import views

urlpatterns = [
    path('', views.allblogs, name="allblogs"),
    # a rota abaixo será: nome_do_app_em_que_urls.py_está/id_do_app/, por exemplo: blog/1/
    path('<int:blog_id>/', views.detail, name="detail"),
]
