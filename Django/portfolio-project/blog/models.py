from django.db import models

class Blog(models.Model):
    title    = models.CharField(max_length=255)
    pub_data = models.DateTimeField()
    body     = models.TextField() # usando text field invés de char field permite receber mais caracteres
    image    = models.ImageField(upload_to='images/')

    # esse método padrão do python, eh como se fosse o toString() do JAVA
    def __str__(self):
        return self.title

    def summary(self):
        return self.body[:100] # retorna os 100 primeiros caracteres de body do 0 ao 99

    def pub_date_pretty(self):
        return self.pub_data.strftime('%b %e %Y')