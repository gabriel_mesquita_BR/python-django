from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
import jobs.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', jobs.views.home, name="home"),
    path('blog/', include('blog.urls')) # essa rota redireciona para o app blog e arquivo urls.py
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # acrescento esse comando para que seja possível
                                                                  # acessar as imagens enviadas pelo usuario a nossa
                                                                  # aplicacao, através da rota: 
                                                                  # localhost:8000/media/images/nome_da_imagem.seu_tipo
