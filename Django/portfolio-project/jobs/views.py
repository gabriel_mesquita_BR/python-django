from django.shortcuts import render

# como a nossa home page terá jobs, colocamos a home page dentro do nosso app jobs

from .models import Job

def home(request):
    jobs = Job.objects # pega todos os jobs cadastrados no bd
    return render(request, 'jobs/home.html', {'jobs':jobs}) # não precisamos especificar o diretório templates