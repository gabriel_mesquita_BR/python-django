from django.db import models

# Após criarmos o model, criamos a migration (python manage.py makemigrations) e executamos 
# ela (python manage.py migrate)

class Job(models.Model):
    # o diretório images armazenará as imagens enviadas pelo usuário
    image   = models.ImageField(upload_to='images/')
    summary = models.CharField(max_length=200)