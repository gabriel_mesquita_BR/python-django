from django.contrib import admin

# registra o model Job para ser exibido na página admin (localhost:8000/admin)

# . => representa o diretório atual (jobs), models => representa o arquivo models.py, Job => é o model
from .models import Job

admin.site.register(Job)