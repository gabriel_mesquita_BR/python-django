from rest_framework import serializers

from profiles_api import models

# quando for trabalhar com POST, PATCH ou PUT é necessário o uso do Serialize

class HelloSerializer(serializers.Serializer):
    """Serializes a name field for testing our APIView"""
    
    # Serialize além de converter JSON para objeto python, também cuidará da validação deste objeto
    
    # crio um input name no nosso serialize HelloSerializer e armazeno o valor em name
    name = serializers.CharField(max_length=10)


# ModelSerializer traz mais funcionalidades que o Serializer para trabalhar com models
# já que este serializer trabalha com o model UserProfile
class UserProfileSerializer(serializers.ModelSerializer):
    """Serializes a user profile object"""

    # abaixo apontamos este Serializer ao model UserProfile
    class Meta:
        model = models.UserProfile

        # passamos a lista de campos ou atributos em nosso model UserProfile que queremos gerenciar aqui no
        # Serializer
        fields = ('id', 'email', 'name', 'password')

        # campo password só queremos usá-lo ao criar um novo usuário ou atualizar um usuário já existente, ou seja, 
        # não queremos usar esse campo qnd o usuário quiser recuperar seus dados, evitando que o password que estará 
        # no formato hash seja enviado para o usuário garantindo maior integridade da senha, logo usamos 
        # extra keywordargs
        extra_kwargs = {
            'password': {
                'write_only': True, # o usuário só poderá usar password para criar ou atualizar um usuário,

                # esta estilização serve somente para a interface que o DRF cria onde é possível fazer todo o CRUD
                # sem acessar o bd diretamente, a interface é parecida com a interface que a ferramenta Swagger 
                # usada para criar documentação de API cria

                # o que for digitado no campo password aparecerá no formato de pontos para ocultar a senha
                'style': {'input_type':'password'}
            } 
        }
    
    # sobrescrevemos essa função do ModelSerializer para que o campo password seja criado como um hash e não no
    # formato plain/text

    # essa função só será chamada após os dados serem validados,tanto é que os dados validados está como parâmetro
    # agora para criar um usuário (User Model) não é mais chamado diretamente create_user de UserProfileManager 
    # que herda de BaseUserManager, é chamado primeiro a função abaixo para a partir dela chama create_user, o que
    # garante a validade dos dados
    def create(self, validated_data):
        """Create and return a new user"""

        # chamamos create_user de UserProfileManager que herda de BaseUserManager
        user = models.UserProfile.objects.create_user(
            email=validated_data['email'],
            name=validated_data['name'],
            password=validated_data['password'],
        )

        return user


class ProfileFeedItemSerializer(serializers.ModelSerializer):
    """Serializes profile feed items"""

    class Meta:
        # seta o ModelSerializer a entidade ProfileFeedItem
        model = models.ProfileFeedItem

        # coloco o id nessa lista de fields para recuperar objetos específicos
        # id e created_on são criados automaticamente, logo os únicos dados que escrevemos são 
        # user_profile e status_text, porém na criação de um feed não queremos que o usuário escreva algo em
        # user_profile, queremos que o valor de user_profile seja o id do usuário que está autenticado, logo
        # colocamos user_profile como somente leitura
        fields = ('id', 'user_profile', 'status_text', 'created_on')
        extra_kwargs = {'user_profile':{'read_only':True}}