from django.urls import path, include
from profiles_api import views

# usado qnd usamos ViewSets para construir a API
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

# no nome da rota n precisamos acrescentar "/" no final, pois ViewSets criará as outras rotas para mim referente aos
# outros verbos HTTP

#base_name servirá para recuperar a url do router abaixo se precisarmos fazer isso usando a função urlretrieve 
#fornecido pelo Django
router.register('hello-viewset', views.HelloViewSet, base_name='hello-viewset')

# n é preciso passar base_name, pois estamos usando nesse ViewSets queryset assim DRF já interpretará o base_name
router.register('profile', views.UserProfileViewSet)

router.register('feed', views.UserProfileFeedViewSet)

urlpatterns = [
    path('hello-view/', views.HelloApiView.as_view(), name='hello-view'),

    path('login/', views.UserLoginApiView.as_view(), name='login'),

    # colocamos no primeiro parâmetro uma string vazia, pois não queremos colocar um prefixo nas uris da nossa API
    # sendo construída através do ViewSets 
    path('', include(router.urls))
]