from rest_framework import permissions

class UpdateOwnProfile(permissions.BasePermission):
    """Allow user to edit their own profile"""

    # o método abaixo será chamado toda vez que for feita uma requisição na API
    # retornado True ou False se o usuário autenticado (logado) tem permissão para acessar ou fazer alteração
    # em determinadas rotas
    def has_object_permission(self, request, view, obj):
        """Check user is trying to edit their own profile"""

        # deixaremos que outros usuários possam ver o perfil de um usuário, mas um usuário só poderá fazer
        # alterações no seu próprio perfil

        # verificamos se o método da requisição feita está dentro de SAFE_METHODS, se estiver permitimos o usuário
        # acessar a rota desejada
        # SAFE_METHODS são métodos que não efetuam alterações de dados, como o verbo HTTP GET, usado apenas para
        # visualizar dados
        if request.method in permissions.SAFE_METHODS:
            return True
        
        # se o método de requisição não for SAFE_METHOD como POST, PUT, PATCH, DELETE então verificamos se o id do 
        # usuário do perfil que está tentando ser acessado é igual ao id do usuário que fez a requisição para acessar 
        # o perfil, para permitir ou não o usuário a acessar o perfil
        return obj.id == request.user.id


class UpdateOwnStatus(permissions.BasePermission):
    """Allow users to update their own status"""

    def has_object_permission(self, request, view, obj):
        """Check the user is trying update their own status"""

        if request.method in permissions.SAFE_METHODS:
            return True
        
        # obj é referente a entidade ProfileFeedItem, user_profile é um foreign key referente a entidade UserProfile
        # e o atributo id pertence a entidade UserProfile

        # verifico se o id do user_profile é igual ao id do usuário autenticado, se for então o usuário tem
        # permissão para efetuar alterações no feed (PUT, PATCH, DELETE)
        return obj.user_profile.id == request.user.id