# from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from profiles_api import serializers

# =============================================== #

from rest_framework import viewsets
from profiles_api import models

# este token que será gerado aleatoriamente será adicionado no header de cada requisição feita pelo usuário,
# caso ele tenha autorização 
from rest_framework.authentication import TokenAuthentication
from profiles_api import permissions
from rest_framework import filters
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

# from rest_framework.permissions import IsAuthenticatedOrReadOnly # o usuário está autenticado ou anônimo
from rest_framework.permissions import IsAuthenticated


class HelloApiView(APIView):
    """Test Api View"""

    # HelloSerializer é a classe serializer que criamos
    # assim qnd a requisição for POST, PUT ou PATCH usaremos o serializer e o cliente deverá passar um
    # nome com no máximo 10 caracteres como foi definido em HelloSerializer
    serializer_class = serializers.HelloSerializer
    
    # format pode ser usado para determinar como o endpoint deve-se iniciar, por exemplo
    # format pode ser usado para acrescentar um determinado queryParam no final da uri, ou seja, como sufixo
    # format pode ser usado para determinar os tipos de dados que aceita vindo da requisição: json, xml, html
    def get(self, request, format=None):
        """Return a list of APIView features"""

        an_apiview = [
            'Uses Http methods function (get, post, put, patch, delete)',
            'Is similar to a traditional Django View',
            'Gives you the most control over your application logic',
            'Is mapped manually to URLS'
        ]

        # para retornar um JSON é necessário a resposta ser uma lista ou dicionário
        # abaixo colocamos como resposta um dicionário
        return Response({'message': 'Hello', 'an_apiview':an_apiview})


    def post(self, request):
        """Create a hello message with our name"""

        # chamamos serializer_class que armazena a classe serializer HelloSerializer e passamos os dados vindos da 
        # requisição para esta classe, assim converte os dados vindo da requisição no formato JSON para objeto PYTHON
        serializer = self.serializer_class(data=request.data)

        # aqui verificaremos se o serializer é válido
        if serializer.is_valid():
            # recuperamos o dado passado para input name, já que ele é o atributo da classe
            name = serializer.validated_data.get('name')

            # abaixo usamos a funcionalidade f string invés do format
            message = f'Hello {name}'
            
            return Response({'message':message})
        else:
            # para dados inválidos usamos o status code 400 (BAD REQUEST)
            # serializer.errors retorna um dicionário de todos os erros baseado nas regras de validação impostas no
            # serializer que criamos
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    
    def put(self, request, pk=None):
        """Handle updating an object"""

        # atualiza todo o objeto
        return Response({'method':'PUT'})

    
    def patch(self, request, pk=None):
        """Handle a partial update of an object"""

        # não atualiza todo o objeto, somente os dados do objeto que vieram no payload, ou seja, que sofreram
        # modificações
        return Response({'method':'PATCH'})


    def delete(self, request, pk=None):
        """Delete an object"""
        return Response({'method':'DELETE'})


class HelloViewSet(viewsets.ViewSet):
    """Test Api ViewSet"""

    serializer_class = serializers.HelloSerializer

    # retorna uma lista de dados através do verbo http GET
    def list(self, request):
        """Return a hello message"""
        a_viewset = [
            'Uses actions(list, create, retrieve, update, partial_update, destroy)',
            'Automatically maps to URLS using Routers',
            'Provides more functionality with less code'
        ]

        return Response({'message':'Hello', 'a_viewset':a_viewset})


    def create(self, request):
        """Create a new hello message"""

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid:
            name = serializer.validated_data.get('name')
            message = f'Hello {name}'

            return Response({'message':message})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    
    # retorna um dado específico através do verbo http GET
    def retrieve(self, request, pk=None):
        """Handle getting an object by its ID"""
        return Response({'http_method':'GET'})

    
    def update(self, request, pk=None):
        """Handle updating an object"""
        return Response({'http_method':'PUT'})

    
    def partial_update(self, request, pl=None):
        """Handle updating part of an object"""
        return Response({'http_method':'PATCH'})

    
    def destroy(self, request, pk=None):
        """Handle removing an object"""
        return Response({'http_method':'DELETE'})


# ModelViewSet possui muito mais funcionalidade que ViewSet, além de ter sido criado especificadamente para
# gerenciar models através de API, porém tbm é um viewsets
class UserProfileViewSet(viewsets.ModelViewSet):
    """Handle creating and updating profiles"""

    serializer_class = serializers.UserProfileSerializer

    # diz ao ModelViewSet os objetos que serão gerenciados pelo viewsets, sendo estes objetos as ações de
    # listar, criar, recuperar um dado específico, atualizar, atualizar parcialmente e remover
    queryset = models.UserProfile.objects.all()

    # assim com os 2 comandos acima o DRF sabe que queremos usar as funções padrões: list, create, retrieve, update, 
    # partial_update e destroy referente ao UserProfile (User Model)

    # eh necessário acrescentar uma vírgula para que seja realmente uma tupla e não somente um item ou atributo
    authentication_classes = (TokenAuthentication,)
    permission_classes     = (permissions.UpdateOwnProfile,)

    # adiciono acima o token (autorização) e as permissões (que definem o que o usuário precisa ter para acessar algo)

    # adiciono filtro para buscar usuários por nome e email
    filter_backends = (filters.SearchFilter,)
    search_fields   = ('name', 'email',)


class UserLoginApiView(ObtainAuthToken):
    """Handle creating user authentication tokens"""

    # iremos sobrescrever ObtainAuthToken para que o login apareça na nossa API Profile, já que por padrão
    # ele só aparece na rota admin
    # lembrando que ObtainAuthToken herda de APIView
    
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class UserProfileFeedViewSet(viewsets.ModelViewSet):
    """Handles creating, reading and updating profile feed items"""

    # uso token para conseguir acessar determinadas rotas
    authentication_classes = (TokenAuthentication,)

    # seto ProfileFeedItemSerializer em nosso ViewSet
    serializer_class       = serializers.ProfileFeedItemSerializer

    queryset               = models.ProfileFeedItem.objects.all()

    permission_classes     = (
        permissions.UpdateOwnStatus,

        # com essa classe do DRF eu garanto que o usuário só poderá acessar rotas que não são readonly referente
        # a entidade ProfileFeedItem como POST, PUT, PATCH e DELETE se estiver autenticado

        # assim evita que um usuário tente criar um feed sem estar autenticado e receba aquela página de erro 
        # informando que ProfileFeedItem.user_profile precisa ser uma instância de UserProfile, e evita isso
        # n aparecendo o formulário
        # já que a parte de atualização do feed (PUT, PATCH e DELETE) a nossa permissão customizada acima trata
        
        # IsAuthenticatedOrReadOnly

        # a diferença entre IsAuthenticated e IsAuthenticatedOrReadOnly é que o IsAuthenticated bloqueia o acesso
        # a todos os endpoints (GET, POST, PUT, PATCH e DELETE) podendo acessá-los somente se estiver autenticado
        # e autorizado, já o IsAuthenticatedOrReadOnly permite ao usuário mesmo não estando autenticado e autorizado
        # ou autenticado, mas não autorizado visualizar endpoints ou rotas com verbo http GET
        # o usuário estará autorizado qnd o token que ele recebe após autenticar-se (logar-se) estiver no
        # cabeçalho de cada requisição feita por ele
        # assim não é possível mais nem visualizar todos os feeds de todos os usuários e nem o meu próprio feed
        # se n estiver autenticado e autorizado

        IsAuthenticated
    )

    # customizamos essa função referente a criação de um objeto que é chamada após uma requisição POST e após a
    # validação dos dados
    def perform_create(self, serializer):
        """Sets the user profile to the logged in user"""

        # seto o id do usuário autenticado (logado) ao atributo user_profile da entidade ProfileFeedItem
        serializer.save(user_profile=self.request.user)