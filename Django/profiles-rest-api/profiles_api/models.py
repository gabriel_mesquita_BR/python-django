from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager

# as 3 bibliotecas acima são necessárias para sobrescrever o model User que vem por padrão no django
# os métodos das classes abaixo utilizados para sobrescrever o model User não são criados por nós eles
# vem das classes que estamos herdando, logo os nomes das classes não importam pode ser qualquer um, porém 
# deve herdar destas classes que estão definidas

# importa o arquivo settings.py localizado em profiles_project
from django.conf import settings


class UserProfileManager(BaseUserManager):
    """Manager for user profiles"""

    # devido a forma que o Django trabalha se vc criar um usuário com password nula (None) n será possível
    # autenticar este usuário, ou seja, logá-lo já que o Django estará esperando uma senha hash e não nula
    def create_user(self, email, name, password=None):
        """Create a new user profile"""
        if not email: # se o email n for passado
            raise ValueError('Usuário deve ter um endereço de email') # lanço um erro de exceção

        email = self.normalize_email(email) # armazeno o email no formato de lowercase

        # criamos o usuário
        user = self.model(email=email, name=name)

        # setamos a senha para o usuário, utilizando um método que irá criptografá-la
        user.set_password(password)

        # django suporta múltiplos bancos de dados, por isso que é uma boa prática especificar o bd que você
        # deseja usar para salvar os dados, assim a sua aplicação tem suporte a múltiplos bancos de dados
        user.save(using=self._db)

        return user

    def create_superuser(self, email, name, password):
        """Create and save a new superuser with given details"""
        user = self.create_user(email, name, password)

        # setamos o usuário como super usuário ou admin
        # is_superuser é criado automaticamente pela classe PermissionsMixin
        user.is_superuser = True
        user.is_staff     = True

        user.save(using=self._db)

        return user


# python trabalha com múltiplas heranças
# a classe abaixo herda de 2 outras classes

class UserProfile(AbstractBaseUser, PermissionsMixin):
    # abaixo introduzimos um doc string, que descreve o que a função faz
    """Database model for users in the system"""
    email     = models.EmailField(max_length=255, unique=True)
    name      = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff  = models.BooleanField(default=False) # verificará se o usuário terá acesso ou não a rota ou painel de admin

    # precisamos fazer isso, pois estamos customizando o User que vem por padrão no Django
    objects = UserProfileManager()

    # os atributos abaixo será para trabalhar no django admin e no sistema de autenticação do django

    # agora invés de aparecer username aparecerá email
    # USERNAME_FIELD é obrigatório por padrão
    USERNAME_FIELD  = 'email'
    REQUIRED_FIELDS = ['name']

    # como estamos criando uma função na classe temos que passar como parâmetrol self
    def get_full_name(self):
        """Retrieve full name of user"""
        return self.name

    def get_short_name(self):
        """Retrieve short name of user"""
        return self.name

    # converte um objeto para string
    def __str__(self):
        """Return string representation of our user"""
        return self.email


class ProfileFeedItem(models.Model):
    """Profile status update"""

    # o id do usuário será a foreign key deste model (entidade)
    # o primeiro parâmetro é o model que representa a foreign key, sendo que poderíamos colocar no formato de
    # string, mas como customizamos o model User é uma boa prática usar o settings para chamar AUTH_USER_MODEL,
    # pois assim podemos definir se queremos usar o model User que vem por padrão do Django ou o customizado por
    # nós apenas alterando o arquivo settings.py, agora se colocarmos no formato string temos que ir em cada
    # ForeignKey e mudar para o model User que vem por padrão do Django ou o customizado por nós
    user_profile = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE # se deletar o usuário, o seu feed tbm será deletado
    )

    status_text = models.CharField(max_length=255)
    created_on  = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Return the model as a string"""
        return self.status_text