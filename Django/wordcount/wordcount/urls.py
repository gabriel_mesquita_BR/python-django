from django.urls import path

# do diretório atual importe view
from . import views

urlpatterns = [
    # rota home => localhost:8000
    path('', views.home, name="home"),
    # é uma boa prática acrescentar "/" após o nome da rota
    # colocando count como valor de name não importa qual seja o nome da rota, passando {% url 'count' %}
    # como valor do atributo action de form, count continuará conseguindo referenciar ou chamar a rota abaixo
    # não importa o seu nome
    path('contador/', views.count, name="count"),
    path('sobre/', views.count, name="about")
]
