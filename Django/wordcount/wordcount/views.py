from django.http import HttpResponse
from django.shortcuts import render
import operator

def home(request):
    # return HttpResponse('Hello'),
    # return render(request, 'home.html', {'msg': 'This is a message'})
    return render(request, 'home.html')

def count(request):
    fulltext = request.GET['fulltext']

    wordList = fulltext.split() # divido o texto em uma lista

    worddictionary = {} # dicionario

    for word in wordList:
        if word in worddictionary:
            worddictionary[word] += 1
        else:
            worddictionary[word] = 1

    # colocando worddictionary.items() transformo dicionario em uma lista
    # operator.itemgetter(1) pega o primeiro item da lista de itens
    # reverse=True defini o tipo de ordenação que será ao contrário, ou seja, do maior para o menor valor
    sortedWords = sorted(worddictionary.items(), key=operator.itemgetter(1), reverse=True)

    return render(request, 'count.html', {'fulltext':fulltext, 'count': len(wordList), 
        'worddictionary': sortedWords})

def about(request):
    return render(request, 'about.html')

# def test(request):
#    return HttpResponse('<h1>Testando</h1>')