from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth

def signup(request):

    if request.method == 'POST':

        if request.POST['password'] == request.POST['confirmPassword']:

            try:
                # verifico se o nome do usuário já existe no bd
                User.objects.get(username=request.POST['username'])
                # se o usuário existir retorno para a view do signup com uma msg de erro
                return render(request, 'accounts/signup.html', {'error':
                    'Usuário {} já existe'.format(request.POST['username'])})

            # se o usuário não existir será lançado um erro de exceção User.DoesNotExist e o except captura esse erro
            except User.DoesNotExist:
                # criamos o usuário no bd
                # User tem os atributos username, email e password, sendo que email pode deixar vazio e como
                # email é o segundo parâmetro do método create_user e não queremos colocar uma string vazia deixamos
                # explícito que request.POST['password'] será armazenado em password através da atribuição
                user = User.objects.create_user(request.POST['username'], password=request.POST['password'])
                # fazemos a autenticação do usuário, assim ele será logado após a sua criação no bd
                auth.login(request, user)
                return redirect('home')
        else:
            return render(request, 'accounts/signup.html', {'error':'Senhas devem combinar'})

    else:
        return render(request, 'accounts/signup.html')

def login(request):

    if request.method == 'POST':

        # verifico se as credenciais passadas pelo usuário estão corretas
        user = auth.authenticate(username=request.POST['username'], password=request.POST['password'])

        # se usuário não for nulo
        if user is not None:
            auth.login(request, user)
            return redirect('home')
        else:
            return render(request, 'accounts/login.html', {'error':'Nome do usuário ou senha estão incorretos'})

    else:
        return render(request, 'accounts/login.html')

def logout(request):
    
    if request.method == 'POST':
        auth.logout(request)
        return redirect('home')