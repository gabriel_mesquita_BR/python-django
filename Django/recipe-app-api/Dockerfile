# python -> imagem base, 3.7-alpine -> nome da tag ou imagem dentro da imagem base
# alpine é uma versão leve do docker que executa o python 3.7
FROM python:3.7-alpine

# defini quem está mantendo a imagem que será criada através deste arquivo
# MAINTAINER gabriel

# MAINTAINER está como obsoleto, sendo recomendado usar LABEL
LABEL responsavel="gabriel"

# com a linha abaixo diz ao python para rodar no modo unbuffered que é recomendado qnd executamos python dentro do
# docker, com isso não permite ao python armazenar o output (saida) no buffer (no cache, na memória)
ENV PYTHONUNBUFFERED 1

# instalaremos as dependências do nosso projeto que estão em requirements.txt

# copiamos as dependências que estão no requirements.txt no diretório recipe-app-api para o arquivo requirements.txt
# localizado na imagem que será criada
COPY ./requirements.txt /requirements.txt

# instalamos o postgres client
# instalamos jpeg-dev para a instalação do Pillow funcionar corretamente no docker
RUN apk add --update --no-cache postgresql-client jpeg-dev

# instalaremos alguns pacotes temporários que serão úteis apenas durante a execução do arquivo requirements.txt e
# com isso após a execução do arquivo requirements.txt podemos removê-los
# assim n teremos dependências extras na nossa imagem docker, deixando-a no menor tamanho possível

# executo a linha abaixo para setar um alias que chamamos de .tmp-build-deps que usaremos para remover todas
# as dependências temporárias de forma fácil e rápida
# iremos colocar as dependências que queremos instalar temporariamente na próxima linha para ficar mais organizado
# por isso colocamos "\"
# essas dependências (gcc libc-dev linux-headers postgresql-dev) abaixo são para poder instalar o postgres client no 
# docker corretamente
# as dependências (musl-dev zlib zlib-dev) são para poder instalar o Pillow no docker corretamente
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev

# instalo as dependências que estão em requirements.txt localizado na imagem que será criada
RUN pip install -r /requirements.txt

# após o arquivo requirements.txt ter sido executado removeremos os pacotes ou dependências temporárias
RUN apk del .tmp-build-deps

# criamos um diretório app na imagem
RUN mkdir /app

# definimos o diretório app como diretório default, assim quando executarmos a nossa aplicação usando docker container
# executará o que estiver dentro do diretório app
WORKDIR /app

# copiamos o que estiver no diretório app da nossa máquina local para o diretório app da imagem
COPY ./app /app

# criamos diretório para armazenar as imagens que foram enviadas pelo usuário
# em Django temos basicamente 2 diretórios media e static que armazena arquivos estáticos
# static é mais usado para armazenar css, js, imagens que serão usadas na nossa aplicação
# media é mais usado para armazenar as imagens ou arquivos de mídia enviados pelo usuário

# o comando -p faz com que os diretórios volume, web, media e static sejam criados caso n exista ainda.
# assim se vc n colocar o comando -p aparecerá uma msg dizendo que o diretório volume n existe

RUN mkdir -p /volume/web/media

RUN mkdir -p /volume/web/static

# criamos um usuário nomeado de user para somente executar a nossa aplicação usando docker
RUN adduser -D user

# mudamos o dono dos diretórios media e static para user (usuário criado acima) para ter permissão de efetuar
# operações nesses diretórios
RUN chown -R user:user /volume/

RUN chmod -R 755 /volume/web

# mudamos para user
# se n criarmos esse usuário a nossa aplicação será executada no docker usando a conta root, podendo qualquer
# pessoa que acabar tendo acesso a nossa aplicação ter controle total sobre a imagem docker como usuário root
USER user

# para criar a imagem docker, vá ao console e estando dentro do projeto (recipe-app-api) digite: sudo docker build .