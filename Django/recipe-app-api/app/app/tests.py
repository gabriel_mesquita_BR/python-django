# quando vc desejar executar testes, Django irá procurar arquivos ou diretórios que comecem com a palavra
# test, e tbm irá procurar as funções que começam com test

from django.test import TestCase

from app.calc import add, subtract

class CalcTests(TestCase):

    def test_add__numbers(self):
        """Test that two numbers are added together"""
        
        # esperamos que a soma dos argumentos seja igual a 11
        self.assertEqual(add(3, 8), 11)

    
    def test_subtract_numbers(self):
        """Test that values are subtracted and returned"""

        self.assertEqual(subtract(5, 11), 6)