from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.conf import settings

import uuid
import os # usado para criar um caminho para o arquivo de imagem recebido pelo usuário 

def recipe_image_file_path(instance, filename):
    """Generate file path for new recipe image"""

    # divido o filename em lista através do . e pego o último item da lista que será a extensão da imagem
    extension = filename.split('.')[-1]

    # uuid4 é a versão 4 do uuid
    filename = f'{uuid.uuid4()}.{extension}'

    return os.path.join('uploads/recipe/', filename)


class UserManager(BaseUserManager):

    # **extra_fields funciona como o spread operator do javascript, ou seja, todos os argumentos que forem
    # passados após password serão armazenados em extra_fields
    def create_user(self, email, password=None, **extra_fields):
        """Creates and saves a new user"""

        # se o email for nulo (None)
        if not email:
            raise ValueError('Usuário deve ter um endereço de e-mail')

        # normalize_email transforma o email em lowercase
        user = self.model(email=self.normalize_email(email), **extra_fields)

        # método abaixo vem de AbstractBaseUser e criptografa a senha
        user.set_password(password)
        user.save(using=self._db)

        return user

    
    def create_superuser(self, email, password):
        """Creates and saves a new superuser"""

        user = self.create_user(email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model that supports using email instead of username"""

    email     = models.EmailField(max_length=255, unique=True)
    name      = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff  = models.BooleanField(default=False) # se tem permissão ou não para acessar o painel admin, se é superuser

    objects = UserManager()

    USERNAME_FIELD = 'email'


class Tag(models.Model):
    """Tag to be used for a recipe"""

    name = models.CharField(max_length=255)

    # passamos a foreign key referente a entidade user
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    """Ingredient to be used in a recipe"""

    name = models.CharField(max_length=255)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
    

class Recipe(models.Model):
    """Recipe object"""

    # aqui por exemplo temos um relacionamento one-to-many
    # muitas receitas estão relacionadas a um usuário, assim como um usuário está relacionado a muitas receitas
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    title = models.CharField(max_length=255)
    time_minutes = models.IntegerField()
    price = models.DecimalField(max_digits=5, decimal_places=2) # preço terá 2 casas decimais

    # link para caso a receita esteja disponível online
    # blank=True torna o atributo link opcional
    link = models.CharField(max_length=255, blank=True)

    # relacionamento many-to-many

    # muitas receitas estão relacionadas a muitos ingredientes, assim como muitos ingredientes estão relacinados
    # a muitas receitas
    # vc pode colocar Ingredient sem usar aspas simples, porém vc tem que garantir que Ingredient está acima de
    # Recipe
    ingredients = models.ManyToManyField('Ingredient')

    # muitas receitas estão relacionadas a muitas categorias, assim como muitas categorias estão relacinadas
    # a muitas receitas
    tags = models.ManyToManyField('Tag')

    # colocando null=True torno image opcional
    image = models.ImageField(null=True, upload_to=recipe_image_file_path)

    def __str__(self):
        return self.title