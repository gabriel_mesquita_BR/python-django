# neste arquivo usaremos Mocking para sobrescrever o comportamento de uma função para que a nossa aplicação
# só tente estabelecer uma conexão com o banco de dados qnd ele estiver realmente rodando e pronto para
# receber conexões, pois qnd utilizamos django, docker e postgresql, pode ser que o django tente estabelecer
# uma conexão com o bd antes de ele estar realmente pronto para receber uma conexão resultando em erro, por
# isso usaremos Mocking para evitar isso

# assim neste arquivo de testes, simularemos se o bd está disponível ou não está disponível quando nós testarmos
# nosso comando que será colocado antes do docker-compose quando precisarmos usá-lo para fazer a nossa aplicação
# só tentar conectar-se ao bd qnd ele estiver pronto, por isso importamos patch
from unittest.mock import patch

# importaremos a biblioteca que nos permite chamar o nosso comando no código fonte
from django.core.management import call_command

# importaremos o erro operacional que o Django lança qnd o bd não está disponível
from django.db.utils import OperationalError

from django.test import TestCase

class CommandTests(TestCase):

    # testaremos qnd chamamos o nosso comando e o bd já está disponível
    def test_wait_for_db_ready(self):
        """Test waiting for db when db is available"""

        # chamamos a função __getitem__ de ConnectionHandler para recuperar o bd e gi é o seu alias
        with patch('django.db.utils.ConnectionHandler.__getitem__') as gi:
            # assim estamos sobrescrevendo essa função
            gi.return_value = True # como retorna True o bd está disponível

            # wait_for_db é o nome do nosso comando ou gerenciador de comandos que criamos
            call_command('wait_for_db')

            # testaremos se a função acima foi chamada uma única vez
            self.assertEqual(gi.call_count, 1)

    
    # tentaremos recuperar o bd 5 vezes e na sexta vez conseguimos recuperar o bd com sucesso e assim o bd estará 
    # disponível
    # isso será possível, pois a cada 1 segundo o ConnectionHandler tentará recuperar o bd, e será a cada 1 segundo,
    # pois esse é o tempo de delay que definimos no nosso comando wait_for_db

    # com o patch como decorator removeremos o delay de 1 segundo, para deixar o nosso teste mais rápido
    # ao usar o patch como decorator temos que acrescentar um parâmetro extra na função, no nosso caso foi nomeado
    # por nós de ts
    @patch('time.sleep', return_value=True)
    def test_wait_for_db(self, ts):
        """Test waiting for db"""

        with patch('django.db.utils.ConnectionHandler.__getitem__') as gi:
            # nas 5 primeiras tentativas de recuperar o bd lança OperationalError e na sexta tentativa é retornado
            # True, assim o bd é recuperado e está disponível
            gi.side_effect = [OperationalError] * 5 + [True]

            call_command('wait_for_db')

            # testaremos se a função acima foi chamada 6 vezes
            self.assertEqual(gi.call_count, 6)