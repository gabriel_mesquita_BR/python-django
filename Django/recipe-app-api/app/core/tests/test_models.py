from django.test import TestCase

# poderíamos importar get_user_model de User model, porém n eh uma boa prática porque vc pode
# querer mudar quem será o seu User model, como fizemos em outro projeto no qual passou a ser UserProfile
from django.contrib.auth import get_user_model

from core import models

from unittest.mock import patch

def sample_user(email='teste@email.com', password='testpass'):
    """Create a sample user"""
    return get_user_model().objects.create_user(email, password)

class ModelTests(TestCase):

    def test_create_user_with_email_successful(self):
        """Test creating a new user with an email is successful"""

        email    = 'teste@email.com'
        password = 'teste1234'
        
        user = get_user_model().objects.create_user(
            email    = email,
            password = password
        )

        self.assertEqual(user.email, email)

        # checa se a senha está correta ou não (true or false)
        self.assertTrue(user.check_password(password))

    
    def test_new_user_email_normalized(self):
        """Test the email for a new user is normalized"""

        # o método normalize_email só transforma para lowercase a segunda parte do email, ou seja, a
        # que vem depois do @
        email = 'test123@EMAIL.com'
        user  = get_user_model().objects.create_user(email, 'test123')

        self.assertEqual(user.email, email.lower())
    

    def test_new_user_invalid_email(self):
        """Test creating user with no email raises error"""

        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test123')
    

    def test_create_new_superuser(self):
        """Test creating a new superuser"""

        user = get_user_model().objects.create_superuser(
            'test123@email.com',
            'test123'
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

    # testa se o objeto tag criado foi convertido para string representacional e é igual ao nome da tag
    def test_tag_str(self):
        """Test the tag string representation"""

        # passamos o usuário que está relacionado a tag e o nome da tag
        tag = models.Tag.objects.create(
            user=sample_user(),
            name='Vegano'
        )

        self.assertEqual(str(tag), tag.name)
    

    def test_ingredient_str(self):
        """Test the ingredient string representation"""

        ingredient = models.Ingredient.objects.create(
            user=sample_user(),
            name='Pepino'
        )

        self.assertEqual(str(ingredient), ingredient.name)
    
    def test_recipe_str(self):
        """Test the recipe string representation"""

        recipe = models.Recipe.objects.create(
            user=sample_user(),
            title='bife e molhos de cogumelos',
            time_minutes=5,
            price=5.00
        )

        self.assertEqual(str(recipe), recipe.title)
    
    @patch('uuid.uuid4')
    def test_recipe_file_name_uuid(self, mock_uuid):
        """Test that image is saved in the correct location"""

        # qnd a função uuid4 for chamada de uuid o valor do atributo uuid abaixo será retornado por padrão já
        # que estamos usando mocking
        uuid = 'test-uuid'
        mock_uuid.return_value = uuid
        
        # o primeiro argumento eh a instância ou model ou entidade, porém neste método podemos colocar None e
        # o segundo argumento eh o nome que demos para a imagem que será enviada pelo usuário, sendo que o uuid
        # q no nosso caso o seu valor é test-uuid substituirá o nome da imagem (myimage) para evitar problemas de 
        # nomes repetidos, porém a extensão .jpg continuará
        file_path = models.recipe_image_file_path(None, 'myimage.jpg')

        # espera-se esse caminho para o arquivo de imagem enviado pelo usuário
        expected_path = f'uploads/recipe/{uuid}.jpg'

        self.assertEqual(file_path, expected_path)

        




