# estes testes são referentes a rota ou painel admin

from django.test import TestCase
from django.contrib.auth import get_user_model

# importamos reverse para ser possível criar urls (rotas) através dos nossos testes unitários
from django.urls import reverse

# importamos Client para que seja possível fazer requisições através dos nossos testes unitários
from django.test import Client


class AdminSiteTests(TestCase):

    # esta função será executada antes de cada teste
    def setUp(self):

        self.client = Client()
        
        # criamos um superusuário
        self.admin_user = get_user_model().objects.create_superuser(
            email='test123@email.com',
            password='password123'
        )

        # usando esse superusuário logamos no painel admin
        self.client.force_login(self.admin_user)
    
        # criamos um usuário comum
        self.user = get_user_model().objects.create_user(
            email='test@email.com',
            password='password123',
            name='Test user full name'
        )
    

    # testa se os usuários criados estão listados no painel admin
    def test_users_listed(self):
        """Test that users are listed on user page"""

        # admin é o app para o qual quero ir e core_user_changelist é a url
        # esta url é definida por padrão na documentação do django-admin
        # com isso a url ou rota é gerada
        url = reverse('admin:core_user_changelist')

        # efetua a requisição na url definida acima através do verbo HTTP GET
        response = self.client.get(url)
        
        # verifico se o nome e email do usuário estão presentes no response, ou seja, na rota que acessamos acima
        # assertContains tbm verifica se o status da resposta foi 200 (OK)
        self.assertContains(response, self.user.name)
        self.assertContains(response, self.user.email)
    

    # testa se a edição de usuários está funcionando no painel de admin vendo se a página de edição de usuários
    # está renderizando corretamente
    # com isso não precisamos testar realizando PUT ou PATCH, colocando assim menos código
    def test_user_change_page(self):
        """Test that the user edit page works"""

        # /admin/core/user/1
        url = reverse('admin:core_user_change', args=[self.user.id])
        response = self.client.get(url)

        # verifica se recebeu status http 200 (OK) após a requisição
        self.assertEqual(response.status_code, 200)


    # testa se a página de criação de usuários está funcionando no painel de admin vendo se a página de criação de 
    # usuários está renderizando corretamente
    def test_create_user_page(self):
        """Test that the create user page works"""

        url = reverse('admin:core_user_add')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)