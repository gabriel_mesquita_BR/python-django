from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from core import models
from django.utils.translation import gettext as _

# agora personalizaremos o user admin 

class UserAdmin(BaseUserAdmin):

    # essa parte é para ser possível visualizar os usuários criados no painel admin
    ordering     = ['id'] # ordenar pelo id do objeto ou id do user admin
    list_display = ['email', 'name'] # listar os users admins pelo email e name

    # essa parte é para a edição dos usuários no painel admin funcionar, já que personalizamos o user admin
    fieldsets = (
        # o primeiro argumento de cada tupla é o título da seção, onde colocamos None é pq n queremos dar
        # título a seção
        (None, {'fields': ('email', 'password')}),
        (_('Personal Info'), {'fields': ('name',)}),
        (
            _('Permissions'),
            {'fields': ('is_active', 'is_staff', 'is_superuser')}
        ),
        (_('Important dates'), {'fields': ('last_login',)})
    )

    # essa parte é para a criação dos usuários no painel admin funcionar
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            # esses são os campos que aparecerão na criação de um usuário no painel admin
            'fields': ('email', 'password1', 'password2')
        }),
    )


# como personalizamos o user admin e estamos registrando User no painel admin temos que passá-lo no register
admin.site.register(models.User, UserAdmin)
admin.site.register(models.Tag)
admin.site.register(models.Ingredient)
admin.site.register(models.Recipe)