from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

# faz requisições a nossa API e checa as respostas
from rest_framework.test import APIClient
from rest_framework import status

# app user e url create
CREATE_USER_URL = reverse('user:create')
# app user e url token
TOKEN_URL       = reverse('user:token')
ME_URL          = reverse('user:me')

# colocando ** aceita vários parâmetros dinamicamente como se fosse o spread operator no js
def create_user(**params):
    return get_user_model().objects.create_user(**params)


# no nome iniciamos com Public apenas para dar a idéia de que neste teste o usuário n precisará estar autenticado
class PublicUserApiTests(TestCase):
    """Test the users API (public)"""

    def setUp(self):
        self.client = APIClient()

        # testa se o usuário foi criado com sucesso
        def test_create_valid_user_success(self):
            """Test creating user with valid payload is successful"""

            payload = {
                'email': 'teste@email.com',
                'password': 'pass123',
                'name': 'Test name'
            }

            response = self.client.post(CREATE_USER_URL, payload)

            # verificamos se o status da resposta foi 201
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

            # verificamos se o usuário foi realmente criado
            user = get_user_model().objects.get(**response.data)

            # checa se a senha está correta ou não (true or false)
            self.assertTrue(user.check_password(payload['password']))

            # n queremos que password seja retornado como resposta após a requisição para criação do usuário, pois
            # é considerado grande vulnerabilidade do sistema, por isso checamos se password n veio na resposta
            self.assertNotIn('password', response.data)

    
        # testaremos se está sendo criado um usuário que já existe
        def test_user_exists(self):
            """Test creating a user that already exists fails"""

            payload = {
                'email': 'teste@email.com',
                'password': 'pass123',
                'name': 'Test name'
            }

            # criamos o usuário
            create_user(**payload)

            # tentaremos criar o msm usuário novamente
            response = self.client.post(CREATE_USER_URL, payload)

            # espera-se que ocorra um erro 400, pois o usuário já existe
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        
        # testaremos se a senha é mt curta, tendo que ter no mínimo 5 caracteres
        def test_password_too_short(self):
            """Test that the password must be more than or equal 5 characters"""

            payload = {'email':'test@email.com', 'password':'pw', 'name': 'Test'}

            response = self.client.post(CREATE_USER_URL, payload)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

            # checaremos se o usuário já existe através do email
            user_exists = get_user_model().objects.filter(
                email=payload['email']
            ).exists()

            # espera-se que user_exists armazene False, ou seja, espera-se que o usuário ainda n exista
            self.assertFalse(user_exists)


        # testaremos se o token foi criado com sucesso
        def test_create_token_for_user(self):
            """Test that a token is created for the user"""

            payload = {'email':'teste@email.com', 'password':'testpass'}

            # crio o usuário
            create_user(**payload)

            # passo os dados do usuário na requisição para a rota do token e assim espero receber como resposta o
            # token de autorização já que os dados do usuário passados são de um usuário que existe no bd
            response = self.client.post(TOKEN_URL, payload)

            # verifico se existe uma key chamada token dentro da resposta para saber se o token veio
            self.assertIn('token', response.data)

            # n precisamos checar se esse token realmente funciona, pois esse teste já é feito pelo DRF

            # espero que o status HTTP da resposta seja 200
            self.assertEqual(response.status_code, status.HTTP_200_OK)


        # testaremos se as credenciais passadas forem inválidas
        def test_create_token_invalid_credentials(self):
            """Test that token is not created if invalid credentials are given"""

            create_user(email='test@email.com', password='testpass')

            payload = {'email':'teste@email.com', 'password':'wrong'}

            response = self.client.post(TOKEN_URL, payload)

            # espera-se que o token n tenha vindo
            self.assertNotIn('token', respose.data)

            # espera-se que o status http seja 400
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        
        # testaremos não criar um token quando o usuário não existir no bd
        def test_create_token_no_user(self):
            """Test that token is not created if user doesn't exist"""

            payload = {'email':'teste@email.com', 'password':'testpass'}

            # observe que estamos tentando criar um token sem ter criado o usuário, logo ele n existirá no bd

            response = self.client.post(TOKEN_URL, payload)

            self.assertNotIn('token', response.data)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        

        # testaremos não criar um token quando o email for inválido e a senha não for passada
        def test_create_token_missing_field(self):
            """Test that email and password are required"""

            response = self.client.post(TOKEN_URL, {'email':'one', 'password':''})

            self.assertNotIn('token', response.data)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


        # testa a obrigatoriedade de autenticação para usuários
        def test_retrieve_user_unauthorized(self):
            """Test that authentication is required for users"""

            response = self.client.get(ME_URL)

            # espero que o status http seja 401, já que a autenticação n foi feita para acessar a rota acima
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


# no nome iniciamos com Private apenas para dar a idéia de que neste teste o usuário precisará estar autenticado
class PrivateUserApiTests(TestCase):
    """Teste API requests that require authentication"""

    def setUp(self):
        self.user = create_user(
            email='teste@email.com',
            password='testpass',
            name='Teste'
        )

        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
    

    # testará a recuperação dos dados do usuário autenticado com sucesso
    def test_retrieve_profile_success(self):
        """Test retrieving profile for logged in used"""
        
        response = self.client.get(ME_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # espera-se que somente name e email sejam retornados como resposta, pois password n deve ser retornado
        # para evitar vulnerabilidades na API
        self.assertEqual(response.data, {
            'name': self.user.name,
            'email': self.user.email
        })

    
    # testa que n deve ser feito HTTP POST já que iremos atualizar o usuário assim deverá ser utilizado PUT ou PATCH
    def test_post_me_not_allowed(self):
        """Test that POST is not allowed on the me url"""

        response = self.client.post(ME_URL, {})

        # espera-se que o status http seja 405
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    
    # testa se a atualização do usuário funciona com sucesso
    def test_update_user_profile(self):
        """Test updating the user profile for authenticated user"""

        payload = {'name':'new name', 'password': 'newpassword'}

        # atualizamos o usuário
        response = self.client.patch(ME_URL, payload)

        # após a atualização do usuário faz o refresh no bd de testes
        self.user.refresh_from_db()

        self.assertEqual(self.user.name, payload['name'])
        self.assertTrue(self.user.check_password(payload['password']))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

# OBS: cada teste é único, os dados de um teste não podem ser acessados em outro teste