from user.serializers import UserSerializer, AuthTokenSerializer
from rest_framework import generics, authentication, permissions

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

# CreateAPIView permite fazer uma API que cria facilmente um objeto no bd usando serialize que iremos prover
# CreateAPIView herda de GenericAPIView que herda de APIView
class CreateUserView(generics.CreateAPIView):
    """Create a new user in the system"""

    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user"""

    serializer_class = AuthTokenSerializer

    # se vc colocar o comando abaixo vc poderá realizar post referente a autenticação do usuário na interface 
    # fornecida pelo DRF no browser, se n fizer isso para realizar a autenticação vc tem que usar curl, postman, 
    # insomnia
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


# classe para a atualização de usuários
class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user"""

    serializer_class = UserSerializer

    authentication_classes = (authentication.TokenAuthentication,)

    # a única permissão que atribuímos é que o usuário precisa estar autenticado
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        """Retrieve and return authentication user"""
        return self.request.user