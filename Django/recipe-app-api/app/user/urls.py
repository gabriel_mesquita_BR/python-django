from django.urls import path
from user import views

# setamos o nome do app, pois isso ajuda a identificar para que app as urls serão criadas através da função reverse 
# usado nos testes
app_name = 'user'

urlpatterns = [
    # já que por baixo dos panos CreateUserView herda de APIView
    path('create/', views.CreateUserView.as_view(), name='create'),
    path('token/', views.CreateTokenView.as_view(), name='token'),
    path('me/', views.ManageUserView.as_view(), name='me'),
]