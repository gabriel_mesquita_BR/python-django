from django.contrib.auth import get_user_model, authenticate
from rest_framework import serializers

# usamos esta classe, pois qnd estamos escrevendo alguma mensagem no código python que será exibido na tela para
# o usuário se adicionarmos uma linguagem extra na nossa API podemos criar o arquivo de linguagem e automaticamente
# converterá todo o texto para a linguagem que foi adicionada
from django.utils.translation import ugettext_lazy as _

# serializer para criar usuário
class UserSerializer(serializers.ModelSerializer):
    """Serializer for the users object"""

    class Meta:
        model = get_user_model()

        # esses campos abaixo serão convertidos para JSON ou objeto PYTHON
        fields = ('email', 'password', 'name')
        extra_kwargs = {'password':{'write_only':True, 'min_length':5}}


    def create(self, validated_data):
        """Create a new user with encrypted password and return it"""

        # se a senha n estiver criptografada a autenticação n funcionará

        return get_user_model().objects.create_user(**validated_data)

    
    def update(self, instance, validated_data):
        """Update a user, setting the password correctly and return it"""

        # na atualização do usuário poderemos usar como verbo HTTP PATCH e não PUT, assim password poderá ou n vir 
        # no payload e ser armazenado em validated_data se os dados forem válidos, portanto usamos pop para remover
        # password de validated_data se password vier no payload, pois n queremos q a senha seja armazenada no
        # formato plain/text e o segundo argumento de pop é None que é o valor default para caso
        # password n exista, pois como podemos usar PATCH e n PUT se n alterarmos o campo password, password n virá
        # no payload
        # OBS: na atualização do usuário se usarmos PATCH n somos obrigados a escrever algo no campo password, mas
        #      se usarmos PUT somos obrigados a colocar algo no campo password msm que seja a msm senha de antes
        password = validated_data.pop('password', None)

        # atualizamos o usuário passando instance que é o model ou entidade e os dados válidos
        user = super().update(instance, validated_data)

        # se password n for nula (None)
        if password:
            # criptografamos a senha
            user.set_password(password)
            user.save()

        return user


# serializer para autenticar usuário
class AuthTokenSerializer(serializers.Serializer):
    """Serializer for the user authentication object"""

    email = serializers.CharField()
    password = serializers.CharField(
        style={'input_type':'password'},

        # por padrão se tiver whitespace na senha o Django irá eliminá-los, mas n queremos isso por isso setamos
        # False
        trim_whitespace=False
    )

    def validate(self, attrs):
        """Validate and authenticate the user"""

        # attrs tem acesso aos atributos definidos acima com suas validações
        email = attrs.get('email')
        password = attrs.get('password')

        # faremos a autenticação se os dados forem válidos
        user = authenticate(
            # tenho acesso ao contexto da requisição
            request=self.context.get('context'),

            # definimos antes que o username será o email
            username=email,
            password=password
        )

        # se a autenticação falhar
        if not user:
            msg = _('Não foi possível autenticar com as credencias fornecidas')
            # lanço o erro
            raise serializers.ValidationError(msg, code='authentication')

        attrs['user'] = user

        return attrs