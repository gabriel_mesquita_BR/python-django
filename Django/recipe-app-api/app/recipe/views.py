from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from core.models import Tag, Ingredient, Recipe
from recipe import serializers

from rest_framework.decorators import action
from rest_framework.response import Response

# crio essa classe para conter os dados em comum de TagViewSet e IngredientViewSet, reduzindo a duplicidade de 
# código
class BaseRecipeAttrViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    """Base viewset for user owned recipe attributes"""
    
    authentication_classes = (TokenAuthentication,)
    permission_classes     = (IsAuthenticated,)

    def get_queryset(self):
        """Return objects for the current authenticated user only"""

        # converto o valor do query params assigned_only de string, pois vem da url, para inteiro e depois de
        # inteiro para booleano, já que a filtragem vai ser de tags e ingredients que foram atribuídos a receitas

        # n converto diretamente de string para boolean, pois se converter 0 será true e não é isso que queremos
        # colocamos um valor default 0, pois se n colocar dará erro de conversão já que como assigned_only n está
        # armazenando nenhum valor no momento ele será None e n eh possível converter None para int
        assigned_only = bool(
            int(self.request.query_params.get('assigned_only', 0))
        )

        queryset = self.queryset

        if assigned_only:
            # com o comando abaixo retorno as tags e ingredients que foram atribuídos a receitas
            queryset = queryset.filter(recipe__isnull=False)

        # acrescento a função distinct() para que se uma tag ou ingredient for adicionado a mais de uma receita,
        # n seja capturado a msm tag ou o msm ingredient mais de 1 vez
        return queryset.filter(user=self.request.user).order_by('-name').distinct()
    
    def perform_create(self, serializer):
        """Create a new object"""
        serializer.save(user=self.request.user)

# queremos listar as tags, por isso usamos ListModelMixin de mixins
# queremos criar as tags, por isso usamos CreateModelMixin de mixins
# usamos mixins, pois n queremos neste caso realizar todo o CRUD da tag, por isso n usamos as configurações 
# padrões do viewsets

# class TagViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
class TagViewSet(BaseRecipeAttrViewSet):
    """Manage tags in the database"""

    # para listar as tags é necessário estar autenticado e autorizado
    # authentication_classes = (TokenAuthentication,)
    # permission_classes     = (IsAuthenticated,)

    queryset               = Tag.objects.all()
    serializer_class       = serializers.TagSerializer

    # para listar as tags referente ao usuário autenticado precisamos adicionar um filtro
    # def get_queryset(self):
    #    """Return objects for the current authenticated user only"""

        # lembrando que conseguimos capturar o usuário autenticado através de self.request.user, pois passamos 
        # TokenAuthentication e IsAuthenticated para authentication_classes e permission_classes, respectivamente
    #    return self.queryset.filter(user=self.request.user).order_by('-name')
    
    # def perform_create(self, serializer):
    #    """Create a new tag"""

        # o foreign key user será o id do usuário que está autenticado
    #    serializer.save(user=self.request.user)


# class IngredientViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
class IngredientViewSet(BaseRecipeAttrViewSet):
    """Manage ingredients in the database"""

    # authentication_classes = (TokenAuthentication,)
    # permission_classes     = (IsAuthenticated,)

    queryset               = Ingredient.objects.all()
    serializer_class       = serializers.IngredientSerializer

    # def get_queryset(self):
    #    """Return objects for the current authenticated user"""
    #    return self.queryset.filter(user=self.request.user).order_by('-name')

    # def perform_create(self, serializer):
    #    serializer.save(user=self.request.user)


# fazemos a classe abaixo herdar de viewsets.ModelViewSet, pois neste caso queremos fazer todo o CRUD
# como RecipeViewSet está herdando de viewsets.ModelViewSet n foi preciso acrescentar nenhum código para
# a atualização via PATCH ou PUT e a deleção via DELETE funcionar corretamente, automaticamente já veio incorporada
class RecipeViewSet(viewsets.ModelViewSet):
    """Manage recipes in the database"""

    serializer_class = serializers.RecipeSerializer
    queryset = Recipe.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    # todas as funções no python são public, porém é uma boa prática colocar um underline antes do nome da função
    # qnd ela tem a intenção de ser privada
    def _params_to_ints(self, qs):
        """Convert a list of string IDs to a list of integers"""

        # coloco os ids que estão no formato de string já que vieram da url em uma lista através do split tendo
        # como caractere separador a vírgula, pego cada item ou id dessa lista e converto para inteiro e no final
        # retorno uma lista de inteiros
        return [int(str_id) for str_id in qs.split(',')]

    def get_queryset(self):
        """Retrieve the recipes for the authenticated user"""

        # pego o valor do query params tags, q neste caso são os ids das tags
        tags = self.request.query_params.get('tags')
        # pego o valor do query params ingredients, q neste caso são os ids dos ingredients
        ingredients = self.request.query_params.get('ingredients')
        queryset = self.queryset

        if tags:
            tags_ids = self._params_to_ints(tags)

            # tags__id__in é uma sintaxe utilizada pelo django para filtrar por uma foreign key
            # assim retorno todas as receitas que possuem uma tag cujo id está dentro de uma lista de ids
            # no formato de inteiro
            queryset = queryset.filter(tags__id__in=tags_ids)

        if ingredients:
            ingredients_ids = self._params_to_ints(ingredients)
            queryset = queryset.filter(ingredients__id__in=ingredients_ids)

        # return self.queryset.filter(user=self.request.user)
        return queryset.filter(user=self.request.user)

    # esta função é usada para recuperar a classe serializer para uma requisição específica
    # no nosso caso essa requisição específica vai ser a requisição para acessar os detalhes de uma receita
    # específica
    # se eu n colocar essa função nunca os dados vindos como resposta da requisição feita serão iguais aos dados do 
    # RecipeDetailSerializer
    def get_serializer_class(self):
        """Return appropriate serializer class"""

        # verifico se a ação é retrieve, pois se for então está sendo feito uma requisição para acessar os
        # detalhes de uma receita específica
        if self.action == 'retrieve':
            return serializers.RecipeDetailSerializer
        elif self.action == 'upload_image':
            return serializers.RecipeImageSerializer
        
        return self.serializer_class
    
    def perform_create(self, serializer):
        """Create a new recipe"""
        serializer.save(user=self.request.user)
    
    # usamos o decorator action para criar funções, ações customizadas
    # colocando detail=True digo que essa ação customizada será para detalhes, assim essa ação customizada
    # para efetuar upload de imagens só estará disponível qnd acessar a rota de detalhes de uma receita específica
    # /api/recipe/recipes/1/upload-image
    @action(methods=['POST'], detail=True, url_path='upload-image')
    def upload_image(self, request, pk=None):
        """Upload an image to a recipe"""

        # retorna o objeto que no nosso caso é recipe que a view está mostrando baseado no id
        recipe = self.get_object()

        serializer = self.get_serializer(
            recipe,
            data=request.data
        )

        # verifico se os dados passados na requisição são válidos
        if serializer.is_valid():
            serializer.save()

            # retorno a resposta
            return Response(
                # serializer.data armazena o id e a imagem, sendo que a imagem n eh propriamente a imagem e sim,
                # sua url
                serializer.data,
                status = status.HTTP_200_OK
            )

        # se os dados passados na requisição são inválidos
        return Response(
            serializer.errors,
            status = status.HTTP_400_BAD_REQUEST
        )