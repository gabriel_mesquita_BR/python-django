from django.urls import path, include
from rest_framework.routers import DefaultRouter

from recipe import views

router = DefaultRouter()

# tags é o nome da rota e n colocamos "/" no final, pois DefaultRouter criará todas as rotas referentes aos verbos
# HTTP (GET, POST, PUT, PATCH, DELETE) automaticamente
router.register('tags', views.TagViewSet)
router.register('ingredients', views.IngredientViewSet)
router.register('recipes', views.RecipeViewSet)

app_name = 'recipe'

urlpatterns = [
    # deixamos a rota vazia, pois todas as rotas serão criadas pelo DefaultRouter
    path('', include(router.urls)),
]