from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Tag, Recipe

from recipe.serializers import TagSerializer

# como estamos usando viewsets a url passa ser o nome do model - a ação
TAGS_URL = reverse('recipe:tag-list')

class PublicTagsApiTests(TestCase):
    """Test the publicly available tags API"""

    def setUp(self):
        self.client = APIClient()

    # testar a obrigatoriedade de autenticação (login) para recuperar as categorias das receitas
    def test_login_required(self):
        """Test that login is required for retrieving tags"""   

        response = self.client.get(TAGS_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


# para testar a api tags o usuário precisa estar autenticado
class PrivateTagsApiTests(TestCase):
    """Test the authorized user tags API"""

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            'teste@email.com',
            'password123'
        )

        self.client = APIClient()
        self.client.force_authenticate(self.user)

    
    def test_retrieve_tags(self):
        """Test retrieving tags"""

        Tag.objects.create(user=self.user, name='Vegano')
        Tag.objects.create(user=self.user, name='Sobremesa')

        response = self.client.get(TAGS_URL)

        # ordena as tags retornadas pelo nome em ordem decrescente
        tags = Tag.objects.all().order_by('-name')

        # se n passassemos many=True o Django interpretaria que queremos serializar um único objeto qnd na
        # verdade queremos serializar uma lista de objetos, por isso colocamos many=True
        serializer = TagSerializer(tags, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # checo se as tags que recebi como resposta são as tags presentes no bd
        self.assertEqual(response.data, serializer.data)
    

    # testo se as tags recuperadas pertencem somente ao usuário autenticado
    def test_tags_limited_to_user(self):
        """Test that tags returned are for the authenticated user"""

        # crio outro usuário
        user2 = get_user_model().objects.create_user(
            'other@email.com',
            'testpass'
        )

        Tag.objects.create(user=user2, name='Frutas')

        tag = Tag.objects.create(user=self.user, name='Comida Afetiva')

        # só esperamos receber na resposta 1 tag que é a de nome Comida Afetiva já que quem está autenticado
        # é self.user e não user2
        response = self.client.get(TAGS_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # espera-se que o comprimento da resposta seja 1, pois estamos esperando receber somente 1 tag
        self.assertEqual(len(response.data), 1)

        # acesso a 1º posição do array da resposta já que uma resposta pode conter vários dados, várias tags e
        # depois acesso a key name para comparar com o name da tag que pertence ao self.user
        self.assertEqual(response.data[0]['name'], tag.name)
    

    # testa se a tag foi criada com sucesso
    def test_create_tag_successful(self):
        """Test creating a new tag"""

        payload = {'name':'Test tag'}

        # crio a tag
        self.client.post(TAGS_URL, payload)
        
        # verifico se a tag existe no bd de testes já que foi criada acima
        exists = Tag.objects.filter(
            user=self.user,
            name=payload['name']
        ).exists()

        # espera-se que a tag exista, ou seja, espera-se que exists armazene True
        self.assertTrue(exists)

    
    # testa a criação de uma tag com nome inválido
    def test_create_tag_invalid(self):
        """Test creating a new tag with invalid payload"""

        payload  = {'name':''}
        response = self.client.post(TAGS_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_tags_assigned_to_recipes(self):
        """Test filtering tags by those assigned to recipes"""

        tag1 = Tag.objects.create(user=self.user, name='Café da manhã')
        tag2 = Tag.objects.create(user=self.user, name='Almoço')

        recipe = Recipe.objects.create(
            title='Ovos na torrada',
            time_minutes=10,
            price=5.00,
            user=self.user
        )

        recipe.tags.add(tag1)

        # o nosso query params é assigned_only e o seu valor é 1 para dizer que será filtrado somente as tags que foram
        # atribuídas a receitas, ou seja, o valor 1 funciona como o boolean true
        response = self.client.get(TAGS_URL, {'assigned_only':1})

        serializer1 = TagSerializer(tag1)
        serializer2 = TagSerializer(tag2)

        self.assertIn(serializer1.data, response.data)
        self.assertNotIn(serializer2.data, response.data)

    
    # se uma mesma tag for adicionada a duas receitas por exemplo, essa mesma tag será capturada 2 vezes na filtragem
    # por isso iremos fazer o teste pra trazer somente 1 vez em casos como esse
    def test_retrieve_tags_assigned_unique(self):
        """Test filtering tags by assigned returns unique items"""

        tag = Tag.objects.create(user=self.user, name='Café da manhã')
        Tag.objects.create(user=self.user, name='Almoço')

        recipe1 = Recipe.objects.create(
            title='Ovos na torrada',
            time_minutes=10,
            price=5.00,
            user=self.user
        )

        recipe1.tags.add(tag)

        recipe2 = Recipe.objects.create(
            title='Panquecas',
            time_minutes=5,
            price=3.00,
            user=self.user
        )

        recipe2.tags.add(tag)

        response = self.client.get(TAGS_URL, {'assigned_only':1})

        # espera-se que o comprimento da resposta seja 1, pois por mais que a nossa tag tenha sido atribuída a 2 receitas
        # é para capturá-la somente 1 vez
        self.assertEqual(len(response.data), 1)