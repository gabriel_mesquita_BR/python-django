from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Recipe, Tag, Ingredient

from recipe.serializers import RecipeSerializer, RecipeDetailSerializer

import tempfile
import os
from PIL import Image # PIL é o Pillow

RECIPES_URL = reverse('recipe:recipe-list')

def image_upload_url(recipe_id):
    """Return URL for recipe image upload"""
    return reverse('recipe:recipe-upload-image', args=[recipe_id])

# /api/recipe/recipes => rota listagem de receitas
# /api/recipe/recipes/1 => rota de detalhes de uma receita específica

# cria uma rota de detalhes de uma receita específica
def detail_url(recipe_id):
    """Return recipe detail URL"""
    return reverse('recipe:recipe-detail', args=[recipe_id])

def sample_tag(user, name='Curso principal'):
    """Create and return a sample tag"""
    return Tag.objects.create(user=user, name=name)

def sample_ingredient(user, name='canela'):
    """Create and return a sample ingredient"""
    return Ingredient.objects.create(user=user, name=name)

# os argumentos a serem passados para este método após user serão adicionados no dicionário params
def sample_recipe(user, **params):
    
    defaults = {
        'title':'Amostra de receita',
        'time_minutes': 10,
        'price': 5.00
    }

    # qualquer atributo que venha a ser passado após user poderá atualizar ou criar algo em defaults
    # por exemplo, se for passado title após user o valor de title colocado após user sobrescreverá o valor de title
    # em defaults, se for passado link após user a key (link) e o valor de link serão criados no dicionário defaults
    defaults.update(params)

    # **defaults é um dicionário que será convertido para argumentos
    return Recipe.objects.create(user=user, **defaults)


class PublicRecipeApiTests(TestCase):
    """Test unauthenticated recipe API access"""

    def setUp(self):
        self.client = APIClient()

    def test_auth_required(self):
        """Test that authentication is required"""

        response = self.client.get(RECIPES_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateRecipeApiTests(TestCase):
    """Test authenticated recipe API access"""

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'teste@email.com',
            'testpass'
        )
        self.client.force_authenticate(self.user)
    
    def test_retrieve_recipes(self):
        """Test retrieving a list of recipes"""

        # crio 2 receitas com os valores que estão no dicionário defaults já que n coloquei nenhum argumento após user
        sample_recipe(user=self.user)
        sample_recipe(user=self.user)

        response = self.client.get(RECIPES_URL)

        recipes = Recipe.objects.all().order_by('-id')

        serializer = RecipeSerializer(recipes, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_recipes_limited_to_user(self):
        """Test retrieving recipes to user"""

        user2 = get_user_model().objects.create_user(
            'other@email.com',
            'testpass'
        )

        sample_recipe(user=user2)
        sample_recipe(user=self.user)

        response = self.client.get(RECIPES_URL)

        recipes = Recipe.objects.filter(user=self.user)
        serializer = RecipeSerializer(recipes, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data, serializer.data)
    
    def test_view_recipe_detail(self):
        """Test viewing a recipe detail"""

        recipe = sample_recipe(user=self.user)
        
        # adicionamos uma categoria a receita devido ao relacionamentro entre elas ser many-to-many
        recipe.tags.add(sample_tag(user=self.user))

        # adicionamos um ingrediente a receita devido ao relacionamentro entre eles ser many-to-many
        recipe.ingredients.add(sample_ingredient(user=self.user))

        # esta função retorna a url para os detalhes de uma receita específica
        url = detail_url(recipe.id)
        response = self.client.get(url)

        serializer = RecipeDetailSerializer(recipe)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
    
    def test_create_basic_recipe(self):
        """Test creating recipe"""

        payload = {
            'title': 'Bolo de chocolate',
            'time_minutes': 30,
            'price': 5.00
        }

        response = self.client.post(RECIPES_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
        # recupero a receita criada
        recipe = Recipe.objects.get(id=response.data['id'])

        # comparo os valores armazenados em payload e em recipe
        # como recipe é um objeto e não dicionário como payload n posso fazer recipe[key] e tbm n posso fazer
        # recipe.key pq recuperaria o nome da key em recipe e n seu valor, por isso uso getattr() para pegar o
        # valor de cada key em recipe e assim efetuar a comparação com o valor da key em payload
        for key in payload.keys():
            self.assertEqual(payload[key], getattr(recipe, key))
    
    def test_create_recipe_with_tags(self):
        """Test creating a recipe with tags"""

        tag1 = sample_tag(user=self.user, name='Vegano')
        tag2 = sample_tag(user=self.user, name='Sobremesa')

        payload = {
            'title': 'cheesecake de abacate e limão',
            'tags': [tag1.id, tag2.id],
            'time_minutes': 60,
            'price': 20.00
        }

        # crio a receita com as duas tags e recebo uma resposta
        response = self.client.post(RECIPES_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        recipe = Recipe.objects.get(id=response.data['id'])

        # retorna todas as tags (categorias) relacionadas a recipe (receita) criada acima
        tags = recipe.tags.all()

        # espera-se que o número de tags relacionadas a receita criada acima seja 2
        self.assertEqual(tags.count(), 2)

        self.assertIn(tag1, tags)
        self.assertIn(tag2, tags)

    def test_create_recipe_with_ingredients(self):
        """Test creating recipe with ingredients"""

        ingredient1 = sample_ingredient(user=self.user, name='camarões')
        ingredient2 = sample_ingredient(user=self.user, name='gengibre')

        payload = {
            'title': 'curry de camarão',
            'ingredients': [ingredient1.id, ingredient2.id],
            'time_minutes': 20,
            'price': 7.00
        }

        response = self.client.post(RECIPES_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        recipe = Recipe.objects.get(id=response.data['id'])

        ingredients = recipe.ingredients.all()

        self.assertEqual(ingredients.count(), 2)

        self.assertIn(ingredient1, ingredients)
        self.assertIn(ingredient2, ingredients)
    
    def test_partial_update_recipe(self):
        """Test update a recipe with patch"""

        recipe = sample_recipe(user=self.user)
        recipe.tags.add(sample_tag(user=self.user))

        new_tag = sample_tag(user=self.user, name='Curry')

        # os dados que estão aki no payload, serão os dados que serão atualizados na receita criada acima
        payload = {
            'title': 'Frango tikka',
            'tags': [new_tag.id]
        }

        url = detail_url(recipe.id)
        self.client.patch(url, payload)

        recipe.refresh_from_db()

        self.assertEqual(recipe.title, payload['title'])

        tags = recipe.tags.all()

        # poderíamos usar count() no lugar de len() se quiséssemos
        self.assertEqual(len(tags), 1)

        self.assertIn(new_tag, tags)

    def test_full_update_recipe(self):
        """Test updating a recipe with put"""

        recipe = sample_recipe(user=self.user)
        recipe.tags.add(sample_tag(user=self.user))

        # PUT atualiza todo um objeto, assim como n colocamos tags dentro de payload a tag que havia sido adicionada
        # ao recipe será removida
        payload = {
            'title': 'Espaguete',
            'time_minutes': 25,
            'price': 5.00
        }

        url = detail_url(recipe.id)
        self.client.put(url, payload)

        recipe.refresh_from_db()

        self.assertEqual(recipe.title, payload['title'])
        self.assertEqual(recipe.time_minutes, payload['time_minutes'])
        self.assertEqual(recipe.price, payload['price'])
        
        # como n colocamos tags dentro do payload espera-se que a tag que havia sido adicionada ao recipe
        # tenha sido removida na atualização via PUT
        tags = recipe.tags.all()
        self.assertEqual(len(tags), 0)
    

class RecipeImageUploadTests(TestCase):
    
    # executa antes dos testes
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'user@email.com',
            'testpass'
        )
        self.client.force_authenticate(self.user)
        self.recipe = sample_recipe(user=self.user)

    
    # executa após os testes
    # garantiremos que o sistema de arquivos do nosso projeto no docker continuará limpo, ou seja, todas as
    # imagens testes que foram enviadas pelo usuário e armazenadas em um arquivo temporário serão removidas
    def tearDown(self):
        self.recipe.image.delete()
    
    def test_upload_image_to_recipe(self):
        """Test uploading an image to recipe"""

        url = image_upload_url(self.recipe.id)

        # crio um arquivo temporário para armazenar as imagens que foram enviadas pelo usuário e
        # coloco como sufixo desses arquivos a extensão .jpg
        with tempfile.NamedTemporaryFile(suffix='.jpg') as ntf:

            # crio a imagem, simulando um upload pelo usuário
            # tamanho é de 10x10 pixels
            img = Image.new('RGB', (10, 10))

            # salvo a imagem no arquivo temporário no formato JPEG
            img.save(ntf, format='JPEG')

            # como salvamos a imagem em um arquivo, usamos o método seek passando 0 como argumento para
            # setar o cursor para o início do arquivo como se vc estivesse acabado de abrir o arquivo
            ntf.seek(0)

            response = self.client.post(url, {'image':ntf}, format='multipart')

        self.recipe.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('image', response.data)

        # verifico se o caminho criado para armazenar as imagens enviadas pelo usuário existe
        self.assertTrue(os.path.exists(self.recipe.image.path))

    def test_upload_image_bad_request(self):
        """Test uploading an invalid image"""

        url = image_upload_url(self.recipe.id)
        response = self.client.post(url, {'image': 'notimage'}, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_filter_recipes_by_tags(self):
        """Test returning recipes with specific tags"""

        recipe1 = sample_recipe(user=self.user, title='Curry vegano')
        recipe2 = sample_recipe(user=self.user, title='Pavê')

        tag1 = sample_tag(user=self.user, name='Vegano')
        tag2 = sample_tag(user=self.user, name='Sobremesa')

        recipe1.tags.add(tag1)
        recipe2.tags.add(tag2)

        recipe3 = sample_recipe(user=self.user, title='Peixe e batatas')

        # faço uma requisição para RECIPES_URL passando como query params tags, assim faço essa requisição
        # filtrando receitas através das tags
        response = self.client.get(RECIPES_URL, {'tags': f'{tag1.id}, {tag2.id}'})

        serializer1 = RecipeSerializer(recipe1)
        serializer2 = RecipeSerializer(recipe2)
        serializer3 = RecipeSerializer(recipe3)

        self.assertIn(serializer1.data, response.data)
        self.assertIn(serializer2.data, response.data)
        self.assertNotIn(serializer3.data, response.data)

    def test_filter_recipes_by_ingredients(self):
        """Test returning recipes with specific ingredients"""

        recipe1 = sample_recipe(user=self.user, title='Curry vegano')
        recipe2 = sample_recipe(user=self.user, title='Pavê')

        ingredient1 = sample_ingredient(user=self.user, name='Pimenta')
        ingredient2 = sample_ingredient(user=self.user, name='Leite condensado')

        recipe1.ingredients.add(ingredient1)
        recipe2.ingredients.add(ingredient2)

        recipe3 = sample_recipe(user=self.user, title='Bife')

        response = self.client.get(RECIPES_URL, {'ingredients': f'{ingredient1.id}, {ingredient2.id}'})

        serializer1 = RecipeSerializer(recipe1)
        serializer2 = RecipeSerializer(recipe2)
        serializer3 = RecipeSerializer(recipe3)

        self.assertIn(serializer1.data, response.data)
        self.assertIn(serializer2.data, response.data)
        self.assertNotIn(serializer3.data, response.data)